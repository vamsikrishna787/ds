class matrix:
    def __init__(self):
        self.directions=[[-1,0],#up
                         [0,1],#right 
                         [1,0],#down
                         [0,-1]#left
                         ]
        self.visited=set()
    
    
    def getvaluematrix(self,input):
        output= self.dfs(input,self.directions,0,0,[])
        return output

  
  
    def dfs(self,matrix,directions,rowval,colval,output):
        if rowval<0 or  rowval>=len(matrix) or colval<0 or colval>=len(matrix[rowval]) or (rowval,colval) in self.visited:
            return
        output.append(matrix[rowval][colval])
        self.visited.add((rowval,colval))
        for i in range(len(directions)):
             self.dfs(matrix,directions,rowval+directions[i][0],colval+directions[i][1],output)
        return output

        
        










inputs= [
  [1, 2, 3, 4, 5],
  [6, 7, 8, 9, 10],
  [11, 12, 13, 14, 15],
  [16, 17, 18, 19, 20]
]
sample= matrix()
print(sample.getvaluematrix(inputs))








