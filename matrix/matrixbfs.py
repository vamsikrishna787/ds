class solution:
    def matrixbfs(self,matrix,directions):
        output=[]
        visited=set()
        temparray=[]
        temparray.append((0,0))
        visited.add((0,0))
        while(len(temparray)>0):
            r,c= temparray.pop()
            output.append(matrix[r][c])
            for i in range(len(directions)):
                row= directions[i][0]+r
                column=directions[i][1]+c
                if row<0 or row>=len(matrix) or column<0 or column>=len(matrix[row]) or (row,column) in visited:
                    continue
                else:
                    temparray.append((row,column))
                    visited.add((row,column))
        return output

        





directions=[[-1,0],#up
                         [0,1],#right 
                         [1,0],#down
                         [0,-1]#left
                         ]

inputs= [
  [1, 2, 3, 4, 5],
  [6, 7, 8, 9, 10],
  [11, 12, 13, 14, 15],
  [16, 17, 18, 19, 20]
]

sample= solution()
print(sample.matrixbfs(inputs,directions))
