from hashlib import new
from pickle import NONE


class Node:
    def __init__(self,value):
        self.data=value
        self.left=None
        self.right= None
class binarytree:
    def __init__(self):
        self.head= None
    
    def insert(self,value):
        newnode= Node(value)
        if self.head==None:
            self.head=newnode
        else:
            temp= self.head
            while(True):
                if temp.data>value:
                    if temp.left==None:
                        temp.left= newnode
                        break
                    temp= temp.left
                else:
                    if temp.right==None:
                        temp.right= newnode
                        break
                    temp= temp.right
 


    def lookup(self,value):
        temp= self.head
        while(temp!=None):
             if temp.data== value:
                print("found")
                return
             else:
                if temp.data>value:
                 temp= temp.left
                else:
                 temp= temp.right
        print("not found")
    



    def printall(self):
        self.printdata(self.head)
    
    def printdata(self,tree):
        if tree!=None:
            print(tree.data)
            self.printdata(tree.left)
            self.printdata(tree.right)
    
    def delete(self,value):
        temp= self.head
        previous= None
        isrightnode=False
        while(temp!=None):
            if temp.data==value:
                if  temp.left==None and temp.right==None and previous!=None:
                    if isrightnode:
                        previous.right= None
                    else:
                          previous.left= None
                elif temp.right==None and  temp.left!=None and previous!=None:
                     if isrightnode:
                        previous.right= temp.left
                     else:
                          previous.left= temp.left
                elif temp.right==None and  temp.left==None and previous==None:
                    temp.data= None
                elif temp.right!=None and temp.left!=None and previous!=None:
                    if temp.right.left==None:
                        if isrightnode:
                         previous.right= temp.right
                        else:
                          previous.left= temp.right
                    else:
                      newtemp= temp.right.left
                      rightmost= newtemp.right
                      leftmost= newtemp.left
                      newnode= None
                      if newtemp.left==None:
                        newnode= temp.right.left
                        temp.right.left= None
                      else:
                        while(leftmost.left.left!=None):
                            newnode= leftmost
                            leftmost = leftmost.left
                        leftmost.left= None
                        newnode.right= temp.right
                        newnode.left= temp.left
                        if isrightnode:
                         previous.right= newnode
                        else:
                          previous.left= newnode
            else:
                previous= temp
                if temp.data>value:
                    temp= temp.left
                else:
                    temp= temp.right
                    isrightnode= True


               

                



    


samplebinarytree=  binarytree()
samplebinarytree.insert(50)
samplebinarytree.insert(25)
samplebinarytree.insert(75)
samplebinarytree.insert(20)
samplebinarytree.insert(30)
samplebinarytree.insert(65)
samplebinarytree.insert(80)
samplebinarytree.printall()
# samplebinarytree.lookup(95)

