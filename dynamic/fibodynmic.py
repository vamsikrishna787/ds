

class fibopractice:
    
 samplecache={}
 counter=0
 def fiborecu(self,input):

    if input<2:
        return input
    self.counter+=1
    if input in self.samplecache:
        return self.samplecache[input]
    else:
        self.samplecache[input]= self.fiborecu(input-1)+self.fiborecu(input-2)
    return self.samplecache[input]

# 0 1 1 2 3 5 8 13

hd= fibopractice()
print(hd.fiborecu(99))
print(hd.counter)
