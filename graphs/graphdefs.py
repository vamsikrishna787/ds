from collections import deque
class Graph:
    def __init__(self):
        self.data= {}
    def addvertex(self,value):
        self.data[value]=[]
    def addedges(self,start,end):
        self.data[start].append(end)
        self.data[end].append(start)

    def sample(self,graph):
        outpy=[]
        seen=set()
        self.dfs(0,graph,outpy,seen)
        return outpy
    def dfs(self,value,graph,outputarray,seen):
        seen.add(value)
        outputarray.append(value)
        temp= graph[value]
        for i in range(len(temp)):
            if temp[i] not in seen:
                self.dfs(temp[i],graph,outputarray,seen)

    








sample= Graph()
sample.addvertex(0)
sample.addvertex(1)
sample.addvertex(2)
sample.addvertex(3)
sample.addvertex(4)
sample.addvertex(5)
sample.addvertex(6)
sample.addvertex(7)
sample.addvertex(8)

sample.addedges(0,1)
sample.addedges(0,3)
sample.addedges(3,2)
sample.addedges(2,8)
sample.addedges(3,4)
sample.addedges(3,5)
sample.addedges(4,6)
sample.addedges(6,7)

print(sample.sample(sample.data))


