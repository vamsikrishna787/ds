class solution:
    
    
    def sample(self,graph):
        alreadvisited=set()
        output=[]
        self.dfs(0,graph,alreadvisited,output)
        return output
    
    def dfs(self,vertex,grpah,visited,output):
        visited.add(vertex)
        output.append(vertex)
        for i in range(len(grpah[vertex])):
            if grpah[vertex][i]==1 and i not in visited:
                self.dfs(i,grpah,visited,output)

       
    

adjacencyMatrix = [
  [0, 1, 0, 1, 0, 0, 0, 0, 0],
  [1, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 1, 0, 0, 0, 0, 1],
  [1, 0, 1, 0, 1, 1, 0, 0, 0],
  [0, 0, 0, 1, 0, 0, 1, 0, 0],
  [0, 0, 0, 1, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 1, 0, 0, 1, 0],
  [0, 0, 0, 0, 0, 0, 1, 0, 0],
  [0, 0, 1, 0, 0, 0, 0, 0, 0]
]

sample= solution()
print(sample.sample(adjacencyMatrix))

