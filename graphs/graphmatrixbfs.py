from collections import deque
class solut:
    def bfs(self,matrix):
        output=[]
        seen=set()
        temparray=deque()
        temparray.append(0)
        while(len(temparray)>0):
            poped= temparray.popleft()
            seen.add(poped)
            output.append(poped)
            for i in range(len(matrix)):
                if i ==poped:
                    for j in range(len(matrix[i])):
                        if matrix[i][j]==1:
                            if j not in seen:
                                temparray.append(j)
        return output


        





adjacencyMatrix = [
  [0, 1, 0, 1, 0, 0, 0, 0, 0],
  [1, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 1, 0, 0, 0, 0, 1],
  [1, 0, 1, 0, 1, 1, 0, 0, 0],
  [0, 0, 0, 1, 0, 0, 1, 0, 0],
  [0, 0, 0, 1, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 1, 0, 0, 1, 0],
  [0, 0, 0, 0, 0, 0, 1, 0, 0],
  [0, 0, 1, 0, 0, 0, 0, 0, 0]
]
sample= solut()
print(sample.bfs(adjacencyMatrix))

