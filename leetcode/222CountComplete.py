# Definition for a binary tree node.


#level number of nodes= 2power levl- 2^0
#levl1 = 2^1=2
#levl2= 2^2=4
#heightof tree= 2^h-1
#
#


from asyncio.windows_events import NULL
from turtle import left, right


class TreeNode(object):
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution(object):
    def __init__(self):
        self.treenode1 = TreeNode(1)
        self.treenode2 = TreeNode(2)
        self.treenode3 = TreeNode(3)
        self.treenode4 = TreeNode(4)
        self.treenode5 = TreeNode(5)
        self.treenode6 = TreeNode(6)
        self.treenode1.left = self.treenode2
        self.treenode1.right= self.treenode3
        self.treenode2.left = self.treenode4
        self.treenode2.right= self.treenode5
        self.treenode3.left= self.treenode6

    def breadthfirts(self, root):
        count = 0
        if root == None:
            return count
        temparray = [root]
        while(len(temparray) > 0):
            count += 1
            samp = temparray.pop()
            if samp.left != None:
                temparray.append(samp.left)
            if samp.right != None:
                temparray.append(samp.right)
        return count

    def depthfirst(self, root):
        if root == None:
            return 0
        return (self.depthfirst(root.left)+self.depthfirst(root.right))+1





    def compute_depth(self, node: TreeNode) -> int:

        d = 0
        while node.left:
            node = node.left
            d += 1
        return d

    def exists(self, idx: int, d: int, node: TreeNode) -> bool:
      
        left, right = 0, 2**d - 1
        for _ in range(d):
            pivot = left + (right - left) // 2
            if idx <= pivot:
                node = node.left
                right = pivot
            else:
                node = node.right
                left = pivot + 1
        return node is not None
        
    def countNodes(self, root: TreeNode) -> int:
        # if the tree is empty
        if not root:
            return 0
        
        d = self.compute_depth(root)
        # if the tree contains 1 node
        if d == 0:
            return 1
        
      
        left, right = 0, 2**d - 1
        while left <= right:
            pivot = left + (right - left) // 2
            if self.exists(pivot, d, root):
                left = pivot + 1
            else:
                right = pivot - 1
        
    
        return (2**d - 1) + left

sample = Solution()
# print(pow(2, 3))
print(sample.countNodes(sample.treenode1))
