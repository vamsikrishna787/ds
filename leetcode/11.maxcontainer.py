class Solution:
    def maxArea(self, height: list[int]):
        maxcontainer=0
        p1=0
        p2= len(height)-1
        while(p1<p2):
            if height[p1]<height[p2]:
                total= (p2-p1)*height[p1]
                maxcontainer= max(total,maxcontainer)
                p1+=1
            else:
                total= (p2-p1)*height[p2]
                maxcontainer= max(total,maxcontainer)
                p2-=1
        return maxcontainer




sample= Solution()
print(sample.maxArea([1,8,6,2,5,4,8,3,7]))