from collections import defaultdict
class Solution():
    def getboxid(self,row,col):
        rowval= row//3*3
        colval= col//3
        return rowval+colval

    def valid(self,box, row, col, num):
        if num in box or num in row or num in col:
            return False
        return True


    def solveSudoku(self, board):
        columndict=[]
        rowdict=[]
        boxdict=[]
       

        rowdict = [defaultdict(int) for i in range(9)]
        columndict = [defaultdict(int) for i in range(9)]
        boxdict = [defaultdict(int) for i in range(9)]

        for j in range(len(board)):
            for k in range(len(board[j])):
                if board[j][k]!='.':
                    boxid= self.getboxid(j,k)
                    val= board[j][k]

                    columndict[k][val]=1
                    rowdict[j][val]=1
                    boxdict[boxid][val]=1
        self.sampledfs(board,columndict,rowdict,boxdict,0,0)
    
    def sampledfs(self,board,columndict,rowdict,boxdict,r,c):
        if r<0 or r>len(board) or c<0 or c>len(board[r]):
            return True
        if board[r][c] == '.':
            for i in range(9):
                numVal = str(i+1)
                board[r][c] = numVal
                boxId = self.getboxid(r, c)
                box = boxdict[boxId]
                row = rowdict[r]
                col = columndict[c]

                if self.valid(box,row,col,numVal):
                    box[numVal] = 1
                    row[numVal] = 1
                    col[numVal] = 1
                    if r==len(board)-1:
                       if (self.sampledfs(board,columndict,rowdict,boxdict,r+1,0)):
                        return True

                    else:
                       if(self.sampledfs(board,columndict,rowdict,boxdict,r,c+1)):
                        return True
                else:
                    board[r][c] = '.'
        else:
            if r==len(board)-1:
                self.sampledfs(board,columndict,rowdict,boxdict,r+1,0)
            else:
                self.sampledfs(board,columndict,rowdict,boxdict,r,c+1)

        return False

        
        



sample= Solution()
sample.solveSudoku( [["5","3",".",".","7",".",".",".","."],["6",".",".","1","9","5",".",".","."],[".","9","8",".",".",".",".","6","."],["8",".",".",".","6",".",".",".","3"],["4",".",".","8",".","3",".",".","1"],["7",".",".",".","2",".",".",".","6"],[".","6",".",".",".",".","2","8","."],[".",".",".","4","1","9",".",".","5"],[".",".",".",".","8",".",".","7","9"]])

        