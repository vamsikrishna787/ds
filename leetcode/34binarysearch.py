def searchRange(input, target):
        left = 0
        right = len(input)-1
        while(left <= right):
            mid = (left+right)//2
            if input[mid] == target:
                leftindex = mid
                rightindex = mid
                while((leftindex>0 and input[leftindex-1] == target) or (rightindex<len(input)-1 and input[rightindex+1] == target)):
                    if leftindex>0 and input[leftindex-1] == target:
                        leftindex -= 1
                    if rightindex<len(input)-1 and  input[rightindex+1] == target:
                        rightindex += 1
                return [leftindex, rightindex]
            elif target < input[mid]:
                right = mid-1
            elif target > input[mid]:
                left = mid+1
        return [-1, -1]


print(searchRange([2,2], 2))
