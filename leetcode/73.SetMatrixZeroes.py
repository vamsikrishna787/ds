class Solution(object):
    def setZeroes(self, matrix):
        samplset=set()
        for i in range(len(matrix)):
            for j in range(len(matrix[i])):
                if matrix[i][j]==0:
                    for k in range(len(matrix[i])):
                        samplset.add((i,k))
                    for l in range(len(matrix)):
                        samplset.add((l,j))
        while (len(samplset)>0):
            r,l= samplset.pop()
            matrix[r][l]=0

        return matrix

sample= Solution()
print(sample.setZeroes([[1,1,1],[1,0,1],[1,1,1]]))

