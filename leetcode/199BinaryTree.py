# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution(object):
    def __init__(self):
        self.treenode1= TreeNode(1)
        self.treenode2= TreeNode(2)
        self.treenode3= TreeNode(3)
        self.treenode4= TreeNode(4)
        self.treenode5= TreeNode(5)
        self.treenode1.left= self.treenode2
        self.treenode1.right= self.treenode3
        self.treenode2.right=self.treenode5
        self.treenode3.right= self.treenode4

        self.maxdepth=0
    

    def rightSideView(self, root):
        output=[]
        if root==None:
            return output
        currentstack= [root]
        while(len(currentstack)>0):
            output.append(currentstack[len(currentstack)-1].val)
            tempstack=[]
            for i in range(len(currentstack)):
                if currentstack[i].left!=None:
                   tempstack.append(currentstack[i].left)
                if currentstack[i].right!=None:
                   tempstack.append(currentstack[i].right)
            currentstack= tempstack
        return output


    def rightsiderecursion(self,root,currentdepth,output):
        if root==None:
            return None
        currentdepth+=1
        if currentdepth>self.maxdepth:
            self.maxdepth= currentdepth
            output.append(root.val)
            self.rightsiderecursion(root.right,currentdepth,output)
            self.rightsiderecursion(root.left,currentdepth,output)
        return output

            
    
            
            


         


        


        



sample= Solution()
print(sample.maxdepthrecursion(sample.treenode1))
print(sample.rightsiderecursion(sample.treenode1,0,[]))



        