from pickle import NONE


class Node:
    def __init__(self, value):
        self.val = value
        self.next = None


class linkedlist:
    def __init__(self):
        self.head = None
        self.tail = None
        self.leth = 0

    def insertlist(self, value):
        newnode = Node(value)
        if self.head == None:
            self.head = newnode
            self.tail = self.head
            self.leth = 1
        else:
            self.tail.next = newnode
            self.tail = newnode
            self.leth += 1

    def mergelist(self, list1, list2):
        if list1 == None:
            return list2
        if list2 == None:
            return list1

        output = None
        tail = None

        set1 = list1.head
        set2 = list2.head
        while True:
            if set1.val > set2.val:

                if output == None:
                    output = set2
                    tail = output
                    set2 = set2.next
                else:
                    tail.next = set2
                    tail = set2
                    set2 = set2.next

            else:

                if output == None:
                    output = set1
                    tail = output
                    set1 = set1.next
                else:

                    tail.next = set1
                    tail = set1
                    set1 = set1.next
            if set1 == None:
                tail.next = set2
                break
            if set2 == None:
                tail.next = set1
                break
        return output


list1 = linkedlist()
list1.insertlist(1)
list1.insertlist(2)
list1.insertlist(4)

list2 = linkedlist()
list2.insertlist(1)
list2.insertlist(3)
list2.insertlist(4)


out = linkedlist()
out.mergelist(list1, list2)
