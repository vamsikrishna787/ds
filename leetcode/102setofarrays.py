# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution(object):
    def __init__(self):
        self.treenode1 = TreeNode(1)
        treenode2 = TreeNode(2)
        treenode3 = TreeNode(3)
        self.treenode1.left = treenode2
        self.treenode1.right = treenode3

    def levelOrder(self, root):
        output = []
        if root == None:
            return output
        tempstack = []
        tempstack.append(root)
        while(len(tempstack) > 0):
            testout = []
            samp = []
            for i in range(len(tempstack)):
                if tempstack[i] != None:
                    samp.append(tempstack[i].val)
                    if tempstack[i].left != None:
                        testout.append(tempstack[i].left)
                    if tempstack[i].right != None:
                        testout.append(tempstack[i].right)
            output.append(samp)
            tempstack = testout
        return output


sample = Solution()
print(sample.levelOrder(sample.treenode1))
