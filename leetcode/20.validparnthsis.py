
class Solution:
    def isValid(self, s):
        tempstack=[]
        for i in range(len(s)):
            if s[i]=="{" or s[i]=="[" or s[i]=="(":
                tempstack.append(s[i])
            else:
                if len(tempstack)==0:
                    return False
                x= tempstack[len(tempstack)-1]
                if x=='{' and s[i]=='}':
                    tempstack.pop(len(tempstack)-1)
                elif x=='[' and s[i]==']':
                     tempstack.pop(len(tempstack)-1)
                elif x=='(' and s[i]==')':
                    tempstack.pop(len(tempstack)-1)
                else:
                     return False
        if len(tempstack)>0:
            return False
        return True


sample= Solution()
sample.isValid(']')


