class solution:
    def __init__(self) :
        self.samcache={}
    def knightProbability(self, n, k, row, column):
        directions=[
  [-2, -1],
  [-2, 1],
  [-1, 2],
  [1, 2],
  [2, 1],
  [2, -1],
  [1, -2],
  [-1, -2]
]
        return (self.sampledfs(directions,0,n,k,row,column))
        

    def sampledfs(self,directions,currentstep,gridsize,targetsteps,currentrow,currentcolumn):

        if currentrow<0 or currentrow>=gridsize or currentcolumn<0 or currentcolumn>=gridsize:
            return 0
        if currentstep== targetsteps:
            return 1
        outp=0
        if (currentrow,currentcolumn,currentstep) in self.samcache:
            return self.samcache[(currentrow,currentcolumn,currentstep)]
        for temprow,tempcolumn in  directions:
            temprow+=currentrow
            tempcolumn+=currentcolumn
            outp+=self.sampledfs(directions,currentstep+1,gridsize,targetsteps,temprow,tempcolumn) *(1/8)
        
        self.samcache[(currentrow,currentcolumn,currentstep)]= outp

        return self.samcache[(currentrow,currentcolumn,currentstep)]




sample= solution()
print(sample.knightProbability(8,30,6,4))