from operator import le


class Solution(object):
    def moveZeroes(self, nums):
        numberofzeros=0
        counter=0
        arrylenth=len(nums)
        while(counter<arrylenth):
            if nums[counter]==0:
                numberofzeros+=1
                nums.pop(counter)
                arrylenth-=1
            else:
                counter+=1
        while numberofzeros>0:
            nums.append(0)
            numberofzeros-=1
        return nums









sample= Solution()
print(sample.moveZeroes([0]))