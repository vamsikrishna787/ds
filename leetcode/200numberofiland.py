from collections import deque


class Solution(object):
    def numIslands(self, grid):
           directions=[[-1,0],[0,1],[1,0],[0,-1]]
           return self.solution(grid,directions)


    def solution(self,grid,directions):
        numberofiland=0
        for r in range(len(grid)):
            for c in range(len(grid[r])):
                if grid[r][c]=="0":
                    continue
                else:
                    numberofiland+=1
                    temparray=deque()
                    temparray.append((r,c))
                    while(len(temparray)>0):
                        row,column= temparray.popleft()
                        grid[row][column]='0'
                        for i in range(len(directions)):
                            ro= directions[i][0]+row
                            co=directions[i][1]+column
                            if ro<0 or ro>=len(grid) or co<0 or co>=len(grid[ro]) or grid[ro][co]=="0" or (ro,co) in temparray:
                                continue
                            else:
                                temparray.append((ro,co))

                
        return numberofiland

            
    



sample= Solution()
print(sample.numIslands([
  ["1","1","0","0","0"],
  ["1","1","0","0","0"],
  ["0","0","1","0","0"],
  ["0","0","0","1","1"]
]))

