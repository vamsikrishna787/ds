from random import sample


class Node:
    def __init__(self, value):
        self.data = value
        self.next = None


class linkedlist:
    def __init__(self):
        node1 = Node(1)
        node2 = Node(2)
        node3 = Node(3)
        node4 = Node(4)
        node1.next = node2
        node2.next = node3
        node3.next = node4
        node4.next = node2
        self.head = node1

    def detectcycle(self):
        temp = self.head
        end = self.findcireculrar(self.head)
        while temp != None and end != None:
            if temp == end:
                return temp
            else:
                temp = temp.next
                end = end.next
        return None

    def findcireculrar(self, head):
        rabit = head
        totoise = head
        while(rabit != None and rabit.next != None):
            rabit = rabit.next.next
            totoise = totoise.next
            if rabit == totoise:
                return totoise
        return None


sample = linkedlist()
print(sample.findcireculrar())
