class Solution:
 def lengthOfLongestSubstring(self,input):
  tempdict={}
  p1=0
  p2=0
  maxlenth=0
  while((len(input))>p2):
    if(input[p2] not in tempdict):
      tempdict[input[p2]]=p2
      maxlenth=max(maxlenth,(p2-p1)+1)
      p2+=1
    else:
      temp=tempdict[input[p2]]
      if(temp>=p1):
       p1=temp+1
      tempdict[input[p2]]=p2
      maxlenth=max(maxlenth,(p2-p1)+1)


      p2+=1

     
  return maxlenth