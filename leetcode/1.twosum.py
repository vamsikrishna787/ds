class Solution:
    def twoSum(self, nums: list[int], target: int):
        tempdict={}
        for i in range(len(nums)):
            if  target-nums[i] in tempdict:
                return [tempdict[target-nums[i]],i]
            else:
                tempdict[nums[i]]=i 
        return [-1,-1]
        


sample= Solution()
print(sample.twoSum([2,7,11,15],9))
