class Node:
    def __init__(self,value):
        self.data= value
        self.next= None
        self.previous= None
        self.child=None

class linkedlist:
    def __init__(self):
        node1= Node(1)
        node2= Node(2)
        node3= Node(3)
        node4= Node(4)
        node5= Node(5)
        node6= Node(6)
        node7= Node(7)
        node8= Node(8)
        node9= Node(9)
        node10= Node(10)
        node11= Node(11)
        node12= Node(12)
        node1.next= node2
        node2.previous= node1
        node2.next= node3
        node3.previous= node2
        node3.next= node4
        node4.previous= node3
        node3.child= node7
        node7.child=node8
        node8.previous=node7
        node7.previous= node3
        self.head= node1
    def flatenlist(self,input):
        temp = input
        while(temp!=None):
            if temp.child!= None:
                childhead= None
                tail=None
                tempchild= temp.child
                while tempchild!=None:
                    if childhead== None:
                        childhead= tempchild
                        tail= childhead
                    else:
                        tail.next= tempchild
                        tempchild.previous= tail.next
                        tail= tempchild
                    tempchild= tempchild.next      
                temp.child=None   
                tail.next=temp.next 
                temp.next.previous=tail 
                childhead.previous=temp
                temp.next=childhead  
            temp= temp.next
        return input
    


sample= linkedlist()
print(sample.head)
sample.flatenlist(sample.head)


    