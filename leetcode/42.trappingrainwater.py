class Solution:
    def maxArea(self, height: list[int]):
       maxleft=0
       maxright=0
       leftpointer=0
       rightpointer= len(height)-1
       total=0
       while(leftpointer<rightpointer):
           if height[leftpointer]<height[rightpointer]:
               if height[leftpointer]>maxleft:
                   maxleft= height[leftpointer]
               else:
                   total+=maxleft- height[leftpointer]
               leftpointer+=1
           else:
               if height[rightpointer]>maxright:
                   maxright= height[rightpointer]
               else:
                   total+=maxright-height[rightpointer]
               rightpointer-=1
       return total
           

s= Solution()
s.maxArea([0,1,0,2,1,0,1,3,2,1,2,1])