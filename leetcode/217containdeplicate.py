def containsDuplicate(self, nums):
        sample={}
        for i in range(len(nums)):
            if nums[i] in sample:
                return True
            else:
                sample[nums[i]]= 1
        return False