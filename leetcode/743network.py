from collections import defaultdict
import heapq
from math import inf


# dihkstr algorithm
#greedy means choose shortest path in options
class Solution:
    def networkDelayTimedij(self, times, n, k):
        donelist=set()
        timelist={}
        adjustlist=defaultdict(list)
        for i in range(n):
            timelist[i+1]= inf
        for source,destination,weight in times:
            adjustlist[source].append((destination,weight))
        tempq=[]
        heapq.heappush(tempq,(0,k))
        timelist[k]=0
        while(tempq):
            weight,source=  heapq.heappop(tempq)
            donelist.add(source)
            processlist= adjustlist[source]
            for destination,weightpro in processlist:
                if destination not in donelist:
                    if timelist[destination]>weightpro+weight:
                         timelist[destination]=weightpro+weight
                         heapq.heappush(tempq,(weightpro+weight,destination))
        
        maxvalue= max(timelist.values())
        if maxvalue==inf:
            return -1
        return maxvalue

# belmonford algorithm
    def networkDelayTimebelmonford(self, times, n, k):
        timedelay={}
        for i in range(n):
            if i+1==k:
              timedelay[i+1]=0
              continue
            timedelay[i+1]=inf

        for j in range(n-1): 
            shufflecounts=0   
            for j in range(len(times)):
                source,destination,weight= times[j]
                if timedelay[source]+weight< timedelay[destination]:
                   timedelay[destination]= timedelay[source]+weight
                   shufflecounts+=1
            if shufflecounts==0:
               break
        maxvalue= max(timedelay.values())
        if maxvalue==inf:
           return -1
        return maxvalue

    

sol = Solution()
n = 5
k = 1
nodes = [1, 2, 3, 4, 5]
times = [[1, 2, 9], [1, 4, 2], [2, 5, -3], [4, 2, -4], [4, 5, 6], [3, 2, 3], [5, 3, 7], [3, 1, 5]]
print(sol.networkDelayTimebelmonford(times, n, k))