from turtle import end_fill


class Node:
    def __init__(self, value):
        self.data = value
        self.next = None


class linkelist:
    def __init__(self):
        self.head = None
        self.tail = None
        self.length = 0

    def append(self, value):
        newnode = Node(value)
        if self.head == None:
            self.head = newnode
            self.tail = self.head
        else:
            self.tail.next = newnode
            self.tail = newnode
        self.length += 1

    def reversepart(self, left, right):
        temp = self.head
        i = 1
        start = None
        reversenode = None
        tail = None
        if self.head.next==None or left==right:
            return self.head
        while(i < self.length):
            if i == left-1:
                start = temp
            if i >= left and i <= right:
              
                if reversenode == None:
                        newnode = Node(temp.data)
                        reversenode = newnode
                        tail = reversenode
                else:
                        newnode = Node(temp.data)
                        newnode.next = reversenode
                        reversenode = newnode
                if i == right:
                    enindex = temp.next
                    start.next, tail.next = reversenode, enindex
                    break
            temp = temp.next
            i += 1


sample = linkelist()
sample.append(3)
sample.append(5)


sample.reversepart(1, 2)
print(sample.head)
