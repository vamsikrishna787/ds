class Solution:
    def isPalindrome(self,input):
     p1=0
     p2= len(input)-1
     while(p1<p2):
        if(input[p1].isalnum() and input[p2].isalnum() ):
            if(input[p1].lower()==input[p2].lower()):
                p1+=1
                p2-=1
                continue
            else:
                return False
        else:
            if(not input[p1].isalnum()):
                p1+=1
            if(not input[p2].isalnum()):
                p2-=1
     return True