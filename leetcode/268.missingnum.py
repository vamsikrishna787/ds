class Solution:
    def missingNumber(self, nums):
        for i in range(len(nums)):
            if i+1 not in  nums:
                return i+1
        return 0



#can solve using n(n+1)/2

sample= Solution()
print(sample.missingNumber([9,6,4,2,3,5,7,0,1]))

        