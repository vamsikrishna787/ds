class Solution(object):
    def transpose(self, matrix):
         m, n = len(matrix), len(matrix[0])
         transpose = []
         for i in range(n):
             newRow = []
             for j in range(m):
                 newRow.append(matrix[j][i])
             transpose.append(newRow)

         return transpose


sample = Solution()
print(sample.transpose([[1, 2, 3], [4, 5, 6], [7, 8, 9]]))
