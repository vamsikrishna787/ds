from collections import deque,defaultdict

class Solution:
    def numOfMinutes(self, n, headID, manager, informTime):
        subordinates = defaultdict(list)
        for idx, employee in enumerate(manager):
            if employee == -1:
                continue
            subordinates[employee].append(idx)
        
        def dfs(managerID):
            maxTime = 0
            for sub in subordinates[managerID]:
                maxTime = max(maxTime, dfs(sub))
            return informTime[managerID] + maxTime

        return dfs(headID)