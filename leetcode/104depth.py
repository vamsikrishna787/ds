# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution(object):
    def __init__(self):
        self.treenode1= TreeNode(1)
        treenode2= TreeNode(2)
        treenode3= TreeNode(3)
        # self.treenode1.left= treenode2
        # self.treenode1.right= treenode3

    def maxdepthrecursion(self,root):
        if root==None:
            return 0
        return max(self.maxdepthrecursion(root.left),self.maxdepthrecursion(root.right))+1


    def maxdepthlop(self,root):
        if root==None:
            return 0
        stack=[]
        depth=0
        stack.append((1,root))
        while(len(stack)>0):
            cuurentdepth,curntrroot= stack.pop()
            if curntrroot!=None:
                depth= max(cuurentdepth,depth)
                stack.append((cuurentdepth+1,curntrroot.left))
                stack.append((cuurentdepth+1,curntrroot.right))
        return depth


        


        



sample= Solution()
print(sample.maxdepthlop(sample.treenode1))



        