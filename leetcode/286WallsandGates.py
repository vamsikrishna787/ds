from collections import deque
class Solution(object):
    def wallsAndGates(self, rooms):
        directions=[[-1,0],[0,1],[1,0],[0,-1]]
        return self.solution(rooms,directions)
    def solution(self,rooms,directions):
       gates=deque()
       for i in range(len(rooms)):
         for j in range(len(rooms[i])):
            if rooms[i][j]==0:
                gates.append((i,j))
       for l in range(len(gates)):
            self.befs(gates[l],rooms,directions)
       return rooms

    def befs(self,gate,rooms,directions):
        row,col= gate
        temaparray=deque()
        temaparray.append((0,row,col))
        visited=set()
        visited.add((row,col))
        while(len(temaparray)>0):
            level,r,c= temaparray.popleft()
            for i in range(len(directions)):
                rowlev= directions[i][0]+r
                collev= directions[i][1]+c
                if rowlev>=0 and rowlev<len(rooms) and collev>=0 and collev<len(rooms[rowlev]) and (rowlev,collev) not in visited and rooms[rowlev][collev]!=0 and rooms[rowlev][collev]!=-1:
                    visited.add((rowlev,collev))
                    rooms[rowlev][collev]=min(level+1, rooms[rowlev][collev])
                    temaparray.append((level+1,rowlev,collev))



       




    











sample= Solution()
print(sample.wallsAndGates([
[2147483647,-1,0,2147483647],
[2147483647,2147483647,2147483647,-1],
[2147483647,-1,2147483647,-1],
[0,-1,2147483647,2147483647]]))

