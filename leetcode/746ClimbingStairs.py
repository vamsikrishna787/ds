
class Solution:
    def __init__(self):
        self.tempcache={}
        
    def minCostClimbingStairs(self, cost):
       return min(self.dfs(len(cost)-1,cost),self.dfs(len(cost)-2,cost))


    def dfs(self,nodeid,costlist):
        if nodeid==0 or nodeid==1:
            return costlist[nodeid]
        if nodeid<0:
            return 0
        if nodeid in self.tempcache.keys():
            return self.tempcache[nodeid]
        output= costlist[nodeid]+min(self.dfs(nodeid-1,costlist),self.dfs(nodeid-2,costlist))
        self.tempcache[nodeid]=output
        return self.tempcache[nodeid]


    def bottoup(self, cost):
        value1=0
        value2=0
        for i in range(len(cost)):
            if i==0:
                value1=cost[i]
                continue
            if i==1:
                value2=cost[i]
                continue
            temp= value2
            value2= cost[i]+min(value1,value2)
            value1= temp
        return min( value2,  value1)
            





       
        
        
        

        


        





sample= Solution()
# print(sample.minCostClimbingStairs([1,100,1,1,1,100,1,1,100,1]))
print(sample.bottoup([1,100,1,1,1,100,1,1,100,1]))

        