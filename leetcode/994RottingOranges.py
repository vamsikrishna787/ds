from collections import deque
class Solution(object):
    def orangesRotting(self, grid):
        directions=[[-1,0],[0,1],[1,0],[0,-1]]
        return self.solution(grid,directions)


    def solution(self,grid,directions):
        freshoranges=0
        dirtyoranges=deque()
        count=0
        # seendirtyoranges=set()
        for i in range(len(grid)):
            for j in range(len(grid[i])):
                if grid[i][j]==1:
                    freshoranges+=1
                if grid[i][j]==2:
                    # seendirtyoranges.add((i,j))
                    dirtyoranges.append((i,j))
        while(len(dirtyoranges)>0):
            temparray=deque()
            for i in range(len(dirtyoranges)):
                r,c= dirtyoranges[i]
                for d in range(len(directions)):
                    row= directions[d][0]+r
                    col= directions[d][1]+c
                    if row<0 or row>=len(grid) or col<0 or col>=len(grid[row]) or grid[row][col]==0 or grid[row][col]==2:
                        continue
                    else:
                        grid[row][col]=2
                        freshoranges-=1
                        temparray.append((row,col))
            if len(temparray)>0:
             count+=1
            dirtyoranges = temparray
        if freshoranges>0:
            return -1
        return count













      

            
    


sample= Solution()
print(sample.orangesRotting([
[2,1,1],[0,1,1],[1,0,1]
]))



        
