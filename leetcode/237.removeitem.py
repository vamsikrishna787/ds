from genericpath import samefile


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
class Solution:
    def __init__(self):
        self.head=None
        self.tail=None
    def inser(self,value):
        newnode= ListNode(value)
        if self.head==None:
            self.head=newnode
            self.tail=self.head
        else:
            self.tail.next=newnode
            self.tail=newnode
    def printlist(self):
        temp= self.head
        while(temp!=None):
            print(temp.val)
            temp= temp.next
    def reverseList(self):
        prevnode=None
        while(self.head!=None):
            temp= self.head
            self.head= self.head.next
            temp.next= prevnode
            prevnode=temp
        self.head=prevnode
    
    def removenode(self,node):
        curr=self.head
        prev=None
        while(curr!=None):
            if curr.val== node:
                prev=None


sample= Solution()
sample.inser(1)
sample.inser(2)
sample.inser(3)
sample.inser(4)
sample.inser(5)

sample.removenode(1)
sample.printlist()