from operator import le


class Solution:
    def reverseString(self, s):
        if len(s)==1:
            return s
        for i in range(len(s)//2):
            s[i],s[len(s)-1-i]=s[len(s)-1-i], s[i]
        return s



sampl= Solution()
print(sampl.reverseString(["h","e","l","l","o"]))

        