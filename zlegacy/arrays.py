thislist = ["apple", "banana", "cherry"]

print(thislist[1])
print(thislist[1:2])
print(len(thislist))
print(type(thislist))


thislist.sort()
thislist.sort(reverse = True)
thislist.reverse()
mylist = thislist.copy()

x = thislist.index('apple')


if "apple" in thislist:
  print("Yes, 'apple' is in the fruits list")
thislist[1] = "blackcurrant"
tropical = ["mango", "pineapple", "papaya"]
thislist.extend(tropical)
thislist.update(tropical)
set3 = thislist.union(tropical)


thislist.pop()
thislist.pop(1)
del thislist[0]
del thislist
thislist.clear()
thislist.remove("banana")



thislist.insert(1,'mango')
thislist.append('cherry')



