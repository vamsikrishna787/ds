

emptyDict = {}
emptyDict['item1'] = 'vamsi'
emptyDict['item1'] = 'vamsi'
emptyDict['item2']='krishna'

if "vamsi" in emptyDict.values():
  print("Yes, 'model' is one of the keys in the thisdict dictionary")



emptyDict.update({'item3': 'manasa'})

print(emptyDict.values())



thisdict = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}

del thisdict["model"]

print(thisdict["brand"])
x = thisdict["model"]
x = thisdict.get("model")
x = thisdict.keys()
x = thisdict.values()
thisdict["year"] = 2020
thisdict.update({"year": 2020})

thisdict.pop("model")
thisdict.popitem()
del thisdict["model"]
thisdict.clear()

print(type(thisdict))

mydict = thisdict.copy()
mydictq = dict(thisdict)

for x in thisdict.keys():
  print(x)

for x in thisdict.values():
  print(x)

for x, y in thisdict.items():
  print(x, y)

if "model" in thisdict:
  print("Yes, 'model' is one of the keys in the thisdict dictionary")