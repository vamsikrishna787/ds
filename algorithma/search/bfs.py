from collections import deque

class Node:
    def __init__(self,value):
        self.data=value
        self.left=None
        self.right= None
class binarytree:
    def __init__(self):
        self.head= None
    
    def insert(self,value):
        newnode= Node(value)
        if self.head==None:
            self.head=newnode
        else:
            temp= self.head
            while(True):
                if temp.data>value:
                    if temp.left==None:
                        temp.left= newnode
                        break
                    temp= temp.left
                else:
                    if temp.right==None:
                        temp.right= newnode
                        break
                    temp= temp.right
  
    def breadthfirstsearch(self):  
        curretnode= self.head 
        outputlist=[]
        temparray=deque()
        temparray.append(curretnode) 
        while(len(temparray)>0):
            curretnode= temparray.popleft()
            outputlist.append(curretnode.data)
            if curretnode.left!=None:
                temparray.append(curretnode.left)
            if curretnode.right!=None:
                 temparray.append(curretnode.right)
        print(outputlist)

    def breadthfirstrec(self,inputarray,outpuarray):
        if len(inputarray)==0:
         return outpuarray
        curentnode= inputarray[0]
        del inputarray[0]
        outpuarray.append(curentnode.data)


        if curentnode.left!=None:
                inputarray.append(curentnode.left)
        if curentnode.right!=None:
                 inputarray.append(curentnode.right)
        return self.breadthfirstrec(inputarray,outpuarray)

    def sample(self):
        print(self.breadthfirstrec([self.head],[]))


         
        

        


            

 


samplebinary= binarytree()

samplebinary.insert(4)
samplebinary.insert(2)
samplebinary.insert(6)



# samplebinary.insert(9)
# samplebinary.insert(4)
# samplebinary.insert(6)
# samplebinary.insert(20)
# samplebinary.insert(170)
# samplebinary.insert(15)
# samplebinary.insert(1)

# samplebinary.breadthfirstsearch()
samplebinary.sample()