class solution:
    def binarysearch(self,input,target):
        left=0
        right= len(input)-1
       
        while(left<=right):
            mid= (left+right)//2
            if input[mid]==target:
                return mid
            elif target<input[mid]:
                right=mid-1
            else:
                left=mid+1
        return -1
    def inital(self,input,target,left,right):
        return self.dfsearch(input,target,left,len(input)-1)


    def dfsearch(self,input,target,left,right):
        if left>right:
            return -1
        mid= (left+right)//2
        if input[mid]==target:
            return mid
        elif target<input[mid]:
            right=mid-1
            return self.dfsearch(input,target,left,right)
        else:
            left=mid+1
            return self.dfsearch(input,target,left,right)
        








s= solution()
print(s.inital([5,7,7,8,9,10],5,0,0))
print(s.binarysearch([5,7,7,8,8,10],9))
