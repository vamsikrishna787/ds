
class Node:
    def __init__(self,value):
        self.data=value
        self.left=None
        self.right= None
class binarytree:
    def __init__(self):
        self.head= None
    
    def insert(self,value):
        newnode= Node(value)
        if self.head==None:
            self.head=newnode
        else:
            temp= self.head
            while(True):
                if temp.data>value:
                    if temp.left==None:
                        temp.left= newnode
                        break
                    temp= temp.left
                else:
                    if temp.right==None:
                        temp.right= newnode
                        break
                    temp= temp.right

    def samplebin(self):
        print(self.dfspostorder(self.head,[]))




    def dfspreorder(self,input,output):
        if input!=None:
            output.append(input.data)
            self.dfspreorder(input.left,output)
            self.dfspreorder(input.right,output)
        return output

  
    def dfsinorder(self,input,output):
        if input!=None:      
            self.dfsinorder(input.left,output)
            output.append(input.data)
            self.dfsinorder(input.right,output)
        return output

    def dfspostorder(self,input,output):
        if input!=None:      
            self.dfspostorder(input.left,output)
            self.dfspostorder(input.right,output)
            output.append(input.data)
        return output






samplebin= binarytree()
samplebin.insert(4)
samplebin.insert(2)
samplebin.insert(6)
samplebin.samplebin()


